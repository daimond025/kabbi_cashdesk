<?php

namespace app\services;

use app\models\Configuration;
use app\models\ModulkassaAuth;
use app\requests\SendReceiptRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use logger\ApiLogger;
use Ramsey\Uuid\Uuid;
use yii\web\ServerErrorHttpException;

class ModulkassaService implements ReceiptServiceInterface
{
    private const SECRET_PLACEHOLDER = '*****';

    private const URL = 'https://service.modulpos.ru/api/fn';
    private const URL_DEBUG = 'https://demo-fn.avanpos.com/fn';

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var SendReceiptRequest
     */
    private $request;

    /**
     * @var ApiLogger
     */
    private $logger;

    public function __construct(ApiLogger $logger)
    {
        $this->logger = $logger;
        $this->init();
    }

    public function init()
    {
        $this->httpClient = new Client();
    }

    public function send(Configuration $configuration, SendReceiptRequest $request)
    {
        $this->configuration = $configuration;
        $this->request       = $request;

        $this->executeDocRequest();
    }

    private function getUrl()
    {
        return (int)$this->configuration->profile->debug === 1 ? self::URL_DEBUG : self::URL;
    }

    private function getAuth()
    {
        $auth = ModulkassaAuth::get($this->configuration->profile_id, $this->configuration->profile->debug);

        if ($auth === null) {
            $data = $this->executeAssociateRequest();
            $auth = new ModulkassaAuth([
                'profile_id' => $this->configuration->profile_id,
                'debug'      => $this->configuration->profile->debug,
                'username'   => $data['userName'],
                'password'   => $data['password'],
            ]);

            if (!$auth->save()) {
                $message = implode(' ', $auth->getFirstErrors());
                throw new ServerErrorHttpException("Get auth data error (Modulkassa): error=\"{$message}\"");
            }
        }

        return $auth;
    }

    private function executeDocRequest()
    {
        $auth = $this->getAuth();

        return $this->executeRequest("{$this->getUrl()}/v1/doc", [
            'auth' => [$auth->username, $auth->password],
            'json' => $this->getRequestData(),
        ], 'Send receipt error (Modulkassa)');
    }

    private function executeAssociateRequest()
    {
        $retailPointUuid = $this->configuration->profile->modulkassaProfile->modulkassa_retail_point_uuid;

        return $this->executeRequest("{$this->getUrl()}/v1/associate/$retailPointUuid", [
            'auth' => [
                $this->configuration->profile->modulkassaProfile->modulkassa_email,
                $this->configuration->profile->modulkassaProfile->modulkassa_password,
            ],
        ], 'Associate retail point error (Modulkassa)');
    }

    private function executeRequest($url, $params, $errorTitle = 'Error')
    {
        try {
            $this->logRequest($url, $params);
            $response = $this->httpClient->post($url, $params);
            $this->logResponse($response);

            return json_decode($response->getBody(), true);
        } catch (BadResponseException $ex) {
            $this->logResponse($ex->getResponse());

            throw new ServerErrorHttpException("$errorTitle: error=\"{$ex->getMessage()}\"");
        } catch (\Exception $ex) {
            throw new ServerErrorHttpException("$errorTitle: error=\"{$ex->getMessage()}\"");
        }
    }

    private function logRequest($url, $params)
    {
        $json = json_encode($this->getFilteredData($params), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->logger->log("Request: url=$url, params=$json");
    }

    private function logResponse($response)
    {
        $responseJson = json_encode($this->getFilteredData(json_decode($response->getBody(), true)),
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->logger->log("Response: $responseJson");
    }

    private function getFilteredData($data)
    {
        if (isset($data['auth'][1])) {
            $data['auth'][1] = self::SECRET_PLACEHOLDER;
        }

        if (isset($data['password'])) {
            $data['password'] = self::SECRET_PLACEHOLDER;
        }

        return $data;
    }

    private function getRequestData()
    {
        $orderIdUuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, $this->request->order_id)->toString();

        return [
            'id'               => $orderIdUuid,
            'checkoutDateTime' => date('c', $this->request->payment_time),
            'docNum'           => $this->request->order_number,
            'docType'          => 'SALE',
            'email'            => empty($this->request->client_email) ? "+{$this->request->client_phone}" : $this->request->client_email,
            'inventPositions'  => [
                [
                    'name'     => $this->configuration->generateProductName($this->request->getPlaceholders()),
                    'price'    => $this->request->cost,
                    'quantity' => 1,
                    'vatTag'   => $this->getVatTag(),
                ],
            ],
            'moneyPositions'   => [
                [
                    'paymentType' => $this->getPaymentType(),
                    'sum'         => $this->request->cost,
                ],
            ],
        ];
    }

    private function getPaymentType()
    {
        return $this->request->payment_type === SendReceiptRequest::PAYMENT_TYPE_CASH
            ? SendReceiptRequest::PAYMENT_TYPE_CASH : SendReceiptRequest::PAYMENT_TYPE_CARD;
    }

    private function getVatTag()
    {
        switch ($this->configuration->tax) {
            case Configuration::TAX_18:
                return 1102;
            case Configuration::TAX_10:
                return 1103;
            case Configuration::TAX_0:
                return 1104;
            case Configuration::TAX_NO:
                return 1105;
            case Configuration::TAX_18_118:
                return 1106;
            case Configuration::TAX_10_110:
                return 1107;
        }
    }
}