<?php

namespace app\services;

use app\models\Configuration;
use app\requests\SendReceiptRequest;

interface ReceiptServiceInterface
{
    public function send(Configuration $configuration, SendReceiptRequest $request);
}