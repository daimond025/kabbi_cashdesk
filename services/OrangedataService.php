<?php

namespace app\services;

use app\models\Configuration;
use app\requests\SendReceiptRequest;
use logger\ApiLogger;
use orangedata\orangedata_client;

class OrangedataService implements ReceiptServiceInterface
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var SendReceiptRequest
     */
    private $request;

    /**
     * @var ApiLogger
     */
    private $logger;

    private const URL = 'https://api.orangedata.ru:12003';
    private const URL_DEBUG = 'https://apip.orangedata.ru:2443';

    private const TYPE = 1;
    private const QUANTITY = 1;
    private const PAYMENT_METHOD_TYPE = 4; // Полный расчет
    private const PAYMENT_SUBJECT_TYPE = 4; // Услуга
    private const PAYMENT_AGENT_TYPE = 4; // Услуга


    public function __construct(ApiLogger $logger)
    {
        $this->logger = $logger;
    }

    public function send(Configuration $configuration, SendReceiptRequest $request)
    {
        $this->configuration = $configuration;
        $this->request       = $request;
        $url                 = $this->getUrl();
        $productName         = $this->configuration->generateProductName($this->request->getPlaceholders());
        $paymentType         = $this->getPaymentType();

        $buyer = new orangedata_client(
            $this->configuration->profile->orangedataProfile->inn,
            $url,
            $this->configuration->profile->orangedataProfile->getSignPKeyFile(),
            $this->configuration->profile->orangedataProfile->getSslClientKeyFile(),
            $this->configuration->profile->orangedataProfile->getSslClientCrtFile(),
            $this->configuration->profile->orangedataProfile->getSslCaCertFile(),
            $this->configuration->profile->orangedataProfile->ssl_client_crt_pass
        );

        $buyer->create_order(
            $this->request->order_id,
            self::TYPE,
            $this->request->client_phone,
            $this->configuration->taxation_system
        );

        $buyer->add_position_to_order(
            self::QUANTITY,
            $this->request->cost,
            $this->configuration->tax,
            $productName,
            self::PAYMENT_METHOD_TYPE,
            self::PAYMENT_SUBJECT_TYPE
        );
        $buyer->add_payment_to_order($paymentType, $this->request->cost);


        $buyer->add_agent_to_order(
            self::PAYMENT_AGENT_TYPE,
            explode(';', $this->configuration->profile->orangedataProfile->payment_transfer_operator_phones),
            $this->configuration->profile->orangedataProfile->payment_agent_operation,
            explode(';', $this->configuration->profile->orangedataProfile->payment_agent_phones),
            explode(';', $this->configuration->profile->orangedataProfile->payment_operator_phones),
            $this->configuration->profile->orangedataProfile->payment_operator_name,
            $this->configuration->profile->orangedataProfile->payment_operator_address,
            $this->configuration->profile->orangedataProfile->payment_operator_inn,
            explode(';', $this->configuration->profile->orangedataProfile->supplier_phones)
        );

        $this->logRequest($url, [
            'inn'                                 => $this->configuration->profile->orangedataProfile->inn,
            'content'                             => [
                'price' => $this->request->cost,
                'tax'   => $this->configuration->tax,
                'text'  => $productName,
            ],
            'checkClose'                          => [
                'payments' => [
                    'type'   => $paymentType,
                    'amount' => $this->request->cost,
                ],
            ],
            'customerContact'                     => $this->request->client_phone,
            'paymentTransferOperatorPhoneNumbers' => explode(';',
                $this->configuration->profile->orangedataProfile->payment_transfer_operator_phones),
            'paymentAgentOperation'               => $this->configuration->profile->orangedataProfile->payment_agent_operation,
            'paymentAgentPhoneNumbers'            => explode(';',
                $this->configuration->profile->orangedataProfile->payment_agent_phones),
            'paymentOperatorPhoneNumbers'         => explode(';',
                $this->configuration->profile->orangedataProfile->payment_operator_phones),
            'paymentOperatorName'                 => $this->configuration->profile->orangedataProfile->payment_operator_name,
            'paymentOperatorAddress'              => $this->configuration->profile->orangedataProfile->payment_operator_address,
            'paymentOperatorINN'                  => $this->configuration->profile->orangedataProfile->payment_operator_inn,
            'supplierPhoneNumbers'                => explode(';',
                $this->configuration->profile->orangedataProfile->supplier_phones),
        ]);
        $response = $buyer->send_order();
        $this->logResponse($response);


    }

    private function getUrl()
    {
        return (int)$this->configuration->profile->debug === 1 ? self::URL_DEBUG : self::URL;
    }

    /**
     * @return integer
     */
    private function getPaymentType()
    {
        return $this->request->payment_type === SendReceiptRequest::PAYMENT_TYPE_CASH ? 1 : 2;
    }

    private function logRequest($url, $params)
    {
        $json = json_encode($this->getFilteredData($params), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->logger->log("Request: url=$url, params=$json");
    }

    private function logResponse($response)
    {
        $responseJson = json_encode($this->getFilteredData(json_decode($response, true)),
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $this->logger->log("Response: $responseJson");
    }

    private function getFilteredData($data)
    {
        return $data;
    }
}