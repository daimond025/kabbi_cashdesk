<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "configuration".
 *
 * @property int     $id
 * @property int     $tenant_id
 * @property int     $city_id
 * @property int     $position_id
 * @property int     $profile_id
 * @property string  $product_name
 * @property int     $sending_condition
 * @property int     $taxation_system
 * @property int     $tax
 * @property int     $created_at
 * @property int     $updated_at
 *
 * @property Profile $profile
 */
class Configuration extends ActiveRecord
{
    public const PLACEHOLDER_SYMBOL = '#';
    public const SCENARIO_UPDATE = 'update';

    public const TAXATION_SYSTEM_OSN = 0;
    public const TAXATION_SYSTEM_USN = 1;
    public const TAXATION_SYSTEM_USN_DIFF = 2;
    public const TAXATION_SYSTEM_ENVD = 3;
    public const TAXATION_SYSTEM_ESN = 4;
    public const TAXATION_SYSTEM_PATENT = 5;

    public const TAX_18 = 1;
    public const TAX_10 = 2;
    public const TAX_18_118 = 3;
    public const TAX_10_110 = 4;
    public const TAX_0 = 5;
    public const TAX_NO = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configuration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['!tenant_id'], 'safe', 'on' => self::SCENARIO_UPDATE],

            [
                [
                    'tenant_id',
                    'city_id',
                    'position_id',
                    'profile_id',
                    'product_name',
                    'sending_condition',
                    'tax',
                ],
                'required',
            ],
            [['tenant_id', 'city_id', 'position_id', 'profile_id', 'created_at', 'updated_at'], 'integer'],
            [['product_name'], 'string', 'max' => 255],
            [
                ['city_id', 'position_id'],
                'unique',
                'targetAttribute' => [
                    'tenant_id', 'city_id', 'position_id'
                ],
                'message' => 'The created combination of the city and the profession already exists.'
            ],
            ['sending_condition', 'integer', 'min' => 0, 'max' => 255],
            [
                'taxation_system',
                'in',
                'range' => [
                    self::TAXATION_SYSTEM_OSN,
                    self::TAXATION_SYSTEM_USN,
                    self::TAXATION_SYSTEM_USN_DIFF,
                    self::TAXATION_SYSTEM_ENVD,
                    self::TAXATION_SYSTEM_ESN,
                    self::TAXATION_SYSTEM_PATENT,
                ],
            ],
            [
                'tax',
                'in',
                'range' => [self::TAX_18, self::TAX_10, self::TAX_18_118, self::TAX_10_110, self::TAX_0, self::TAX_NO],
            ],
            [
                ['profile_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Profile::class,
                'targetAttribute' => ['profile_id' => 'id'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'tenant_id'         => 'Tenant ID',
            'city_id'           => 'City ID',
            'position_id'       => 'Position ID',
            'profile_id'        => 'Profile ID',
            'product_name'      => 'Product Name',
            'sending_condition' => 'Sending Condition',
            'taxation_system'   => 'Taxation System',
            'tax'               => 'Tax',
            'created_at'        => 'Created At',
            'updated_at'        => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'profile_id']);
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id'                => 'id',
            'tenant_id'         => 'tenant_id',
            'city_id'           => 'city_id',
            'position_id'       => 'position_id',
            'profile_id'        => 'profile_id',
            'product_name'      => 'product_name',
            'sending_condition' => 'sending_condition',
            'taxation_system'   => 'taxation_system',
            'tax'               => 'tax',
        ];
    }

    /**
     * @param $tenantId
     * @param $cityId
     * @param $positionId
     *
     * @return array|null|ActiveRecord|Configuration
     */
    public static function get($tenantId, $cityId, $positionId)
    {
        return self::find()
            ->alias('t')
            ->joinWith('profile as c')
            ->where([
                'c.tenant_id'   => $tenantId,
                't.city_id'     => $cityId,
                't.position_id' => $positionId,
            ])
            ->one();
    }

    public function generateProductName(array $placeholders)
    {
        $productName = $this->product_name;
        foreach ($placeholders as $name => $value) {
            $pattern = self::PLACEHOLDER_SYMBOL . $name . self::PLACEHOLDER_SYMBOL;
            $productName = mb_eregi_replace($pattern, $value, $productName);
        }

        return $productName;
    }
}