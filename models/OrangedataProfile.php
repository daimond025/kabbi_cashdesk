<?php

namespace app\models;

use app\components\creatorFile\CreatorFile;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orangedata_profile".
 *
 * @property int    $id
 * @property int    $profile_id
 * @property string $inn
 * @property string $sign_pkey
 * @property string $ssl_client_key
 * @property string $ssl_client_crt
 * @property string $ssl_ca_cert
 * @property string $ssl_client_crt_pass
 *
 * @property string $payment_transfer_operator_phones
 * @property string $payment_agent_operation
 * @property string $payment_agent_phones
 * @property string $payment_operator_phones
 * @property string $payment_operator_name
 * @property string $payment_operator_address
 * @property string $payment_operator_inn
 * @property string $supplier_phones
 *
 * @property int    $created_at
 * @property int    $updated_at
 */
class OrangedataProfile extends ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    protected $_creatorFile;
    protected $_signPKeyFile;
    protected $_sslClientKeyFile;
    protected $_sslClientCrtFile;
    protected $_sslCaCertFile;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orangedata_profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['!profile_id'], 'safe', 'on' => self::SCENARIO_UPDATE],
            [
                ['inn', 'sign_pkey', 'ssl_client_key', 'ssl_client_crt', 'ssl_ca_cert'],
                'required',
            ],
            [
                [
                    'inn',
                    'ssl_client_crt_pass',
                    'payment_agent_operation',
                    'payment_operator_name',
                    'payment_operator_inn',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'payment_operator_address',
                    'payment_transfer_operator_phones',
                    'payment_agent_phones',
                    'payment_operator_phones',
                    'supplier_phones',
                ],
                'string',
            ],
            [
                ['sign_pkey', 'ssl_client_key', 'ssl_client_crt', 'ssl_ca_cert'],
                'safe',
            ],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                               => 'ID',
            'profile_id'                       => 'Profile ID',
            'inn'                              => 'INN',
            'sign_pkey'                        => 'Private key',
            'ssl_client_key'                   => 'Client private key for ssl',
            'ssl_client_crt'                   => 'Client certificate for ssl',
            'ssl_ca_cert'                      => 'Cacert for ssl',
            'ssl_client_crt_pass'              => 'Password for client certificate for ssl',
            'payment_agent_operation'          => 'Телефон оператора перевода',
            'payment_transfer_operator_phones' => 'Операция платежного агента',
            'payment_agent_phones'             => 'Телефон платежного агента',
            'payment_operator_phones'          => 'Телефон оператора по приему платежей',
            'payment_operator_name'            => 'Наименование оператора перевода',
            'payment_operator_address'         => 'Адрес оператора перевода',
            'payment_operator_inn'             => 'ИНН оператора перевода',
            'supplier_phones'                  => 'Телефон поставщика',
            'created_at'                       => 'Created At',
            'updated_at'                       => 'Updated At',
        ];
    }

    public function getSignPKeyFile()
    {

        if (is_null($this->_signPKeyFile)) {
            $this->_signPKeyFile = $this->getCreatorFile()
                ->createTmpFile($this->sign_pkey);
        }

        return $this->_signPKeyFile;
    }

    public function getSslClientKeyFile()
    {
        if (is_null($this->_sslClientKeyFile)) {
            $this->_sslClientKeyFile = $this->getCreatorFile()
                ->createTmpFile($this->ssl_client_key);
        }

        return $this->_sslClientKeyFile;
    }

    public function getSslClientCrtFile()
    {
        if (is_null($this->_sslClientCrtFile)) {
            $this->_sslClientCrtFile = $this->getCreatorFile()
                ->createTmpFile($this->ssl_client_crt);
        }

        return $this->_sslClientCrtFile;
    }


    public function getSslCaCertFile()
    {
        if (is_null($this->_sslCaCertFile)) {
            $this->_sslCaCertFile = $this->getCreatorFile()
                ->createTmpFile($this->ssl_ca_cert);
        }

        return $this->_sslCaCertFile;
    }

    protected function getCreatorFile()
    {
        if (is_null($this->_creatorFile)) {
            $this->_creatorFile = new CreatorFile();
        }

        return $this->_creatorFile;
    }
}
