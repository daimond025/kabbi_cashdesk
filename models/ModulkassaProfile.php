<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "modulkassa_profile".
 *
 * @property int    $id
 * @property int    $profile_id
 * @property string $modulkassa_email
 * @property string $modulkassa_password
 * @property string $modulkassa_retail_point_uuid
 * @property int    $created_at
 * @property int    $updated_at
 */
class ModulkassaProfile extends ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%modulkassa_profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['!profile_id'], 'safe', 'on' => self::SCENARIO_UPDATE],
            [
                [
                    'modulkassa_email',
                    'modulkassa_password',
                    'modulkassa_retail_point_uuid',
                ],
                'required',
            ],
            [['created_at', 'updated_at'], 'integer'],
            [
                ['modulkassa_email', 'modulkassa_password', 'modulkassa_retail_point_uuid'],
                'string',
                'max' => 255,
            ],
            ['modulkassa_email', 'email'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                           => 'ID',
            'profile_id'                   => 'Profile ID',
            'modulkassa_email'             => 'Modulkassa Email',
            'modulkassa_password'          => 'Modulkassa Password',
            'modulkassa_retail_point_uuid' => 'Modulkassa Retail Point Uuid',
            'created_at'                   => 'Created At',
            'updated_at'                   => 'Updated At',
        ];
    }
}
