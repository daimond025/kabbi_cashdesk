<?php

namespace app\models;

interface ProfileModelInterface
{
    public function setAttributes($attributes);

    public function getAttributes();

    public function save();

    public function delete();

    /**
     * @param $profileId
     *
     * @return ProfileModelInterface
     */
    public static function get($profileId);
}