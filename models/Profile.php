<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property int               $id
 * @property int               $tenant_id
 * @property string            $name
 * @property int               $type_id
 * @property int               $debug
 * @property int               $created_at
 * @property int               $updated_at
 *
 * @property ModulkassaProfile $modulkassaProfile
 * @property OrangedataProfile $orangedataProfile
 */
class Profile extends ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['!tenant_id', '!type_id'], 'safe', 'on' => self::SCENARIO_UPDATE],

            [
                [
                    'tenant_id',
                    'name',
                    'type_id',
                    'debug',
                ],
                'required',
            ],
            [['tenant_id', 'type_id', 'created_at', 'updated_at', 'debug'], 'integer'],
            ['debug', 'in', 'range' => [0, 1]],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['name'],
                'unique',
                'targetAttribute' => ['tenant_id', 'name'],
                'message'         => 'This name is already in use',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'type_id'    => 'Type ID',
            'debug'      => 'Debug',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fields()
    {
        return [
            'id'        => 'id',
            'tenant_id' => 'tenant_id',
            'name'      => 'name',
            'type_id'   => 'type_id',
            'debug'     => 'debug',
        ];
    }

    public function getModulkassaProfile()
    {
        return $this->hasOne(ModulkassaProfile::class, ['profile_id' => 'id']);
    }

    public function getOrangedataProfile()
    {
        return $this->hasOne(OrangedataProfile::class, ['profile_id' => 'id']);
    }
}
