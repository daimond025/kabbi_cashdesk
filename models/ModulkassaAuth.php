<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "modulkassa_auth".
 *
 * @property int     $id
 * @property int     $profile_id
 * @property int     $debug
 * @property string  $username
 * @property string  $password
 * @property int     $created_at
 * @property int     $updated_at
 *
 * @property Profile $profile
 */
class ModulkassaAuth extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modulkassa_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'debug', 'username', 'password'], 'required'],
            [['profile_id', 'debug', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password'], 'string', 'max' => 255],
            ['debug', 'in', 'range' => [0, 1]],
            ['profile_id', 'unique'],
            [
                ['profile_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ModulkassaProfile::class,
                'targetAttribute' => ['profile_id' => 'profile_id'],
            ],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'profile_id' => 'Profile ID',
            'debug'      => 'Debug',
            'username'   => 'Username',
            'password'   => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'profile_id']);
    }

    public static function get($profileId, $debug)
    {
        return self::find()
            ->where(['profile_id' => $profileId, 'debug' => $debug])
            ->one();
    }
}
