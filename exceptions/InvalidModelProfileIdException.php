<?php

namespace app\exceptions;

use yii\base\Exception;

class InvalidModelProfileIdException extends Exception
{

    public function getName()
    {
        return 'Invalid type id';
    }

}
