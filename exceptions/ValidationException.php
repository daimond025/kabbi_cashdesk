<?php

namespace app\exceptions;

use yii\web\BadRequestHttpException;

class ValidationException extends BadRequestHttpException
{
    private const DEFAULT_MESSAGE = 'Validation error';

    public function __construct(array $errors, $code = 0, \Exception $previous = null)
    {
        $items = [];
        foreach ($errors as $key => $value) {
            $items[] = "$key|$value";
        }

        parent::__construct(empty($items) ? self::DEFAULT_MESSAGE : implode(';', $items), $code, $previous);
    }
}
