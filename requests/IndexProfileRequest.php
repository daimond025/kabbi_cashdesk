<?php

namespace app\requests;

use yii\base\Model;

class IndexProfileRequest extends Model
{
    public $tenant_id;

    public function rules()
    {
        return [
            ['tenant_id', 'required'],
            ['tenant_id', 'integer'],
        ];
    }
}