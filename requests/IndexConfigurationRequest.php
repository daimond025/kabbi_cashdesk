<?php

namespace app\requests;

use yii\base\Model;

class IndexConfigurationRequest extends Model
{
    public $tenant_id;

    public $city_id;

    public $position_id;

    public function rules()
    {
        return [
            ['tenant_id', 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
        ];
    }
}