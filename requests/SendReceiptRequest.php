<?php

namespace app\requests;

use yii\base\Model;

class SendReceiptRequest extends Model
{
    public const PAYMENT_TYPE_CASH = 'CASH';
    public const PAYMENT_TYPE_CARD = 'CARD';

    public $tenant_id;

    public $city_id;

    public $position_id;

    public $order_id;

    public $order_number;

    public $client_phone;

    public $client_email;

    public $payment_type;

    public $payment_time;

    public $cost;

    public $car_number;

    public function rules()
    {
        return [
            [
                [
                    'tenant_id',
                    'city_id',
                    'position_id',
                    'order_id',
                    'order_number',
                    'client_phone',
                    'payment_type',
                    'payment_time',
                    'cost',
                ],
                'required',
            ],
            [['tenant_id', 'city_id', 'position_id', 'order_id', 'order_number', 'payment_time'], 'integer'],
            [['client_phone', 'payment_type', 'car_number'], 'string'],
            ['client_email', 'email'],
            ['cost', 'double', 'min' => 0],
        ];
    }

    public function getPlaceholders()
    {
        return [
            'order_id'     => $this->order_id,
            'order_number' => $this->order_number,
            'client_phone' => $this->client_phone,
            'client_email' => $this->client_email,
            'cost'         => $this->cost,
            'car_number'   => $this->car_number,
        ];
    }
}