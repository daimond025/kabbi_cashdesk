<?php

namespace logger\handlers;

use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\Response;

/**
 * Class BasicHandler
 * @package logger\handlers
 */
class BasicHandler implements LogHandlerInterface
{
    private const SECRET_PLACEHOLDER = '*****';

    public function process(Request $request, Response $response)
    {
        $preparedRequestParams = $this->getPreparedRequestParams($request);

        return "({$preparedRequestParams})";
    }

    private function isSecretParam($name)
    {
        return mb_stripos($name, 'password') !== false;
    }

    private function getPreparedRequestParams(Request $request)
    {
        $result = [];
        foreach (ArrayHelper::merge($request->getQueryParams(), $request->getBodyParams()) as $name => $value) {
            if ($this->isSecretParam($name)) {
                $value = self::SECRET_PLACEHOLDER;
            }
            $result[] = "$name=\"$value\"";
        }

        return implode(',', $result);
    }
}