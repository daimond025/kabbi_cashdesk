<?php

namespace logger\handlers;

use yii\web\Request;
use yii\web\Response;

/**
 * Interface LogHandlerInterface
 * @package logger\handlers
 */
interface LogHandlerInterface
{
    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return mixed
     */
    public function process(Request $request, Response $response);
}