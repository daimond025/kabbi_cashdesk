<?php

namespace logger\targets;

use yii\base\BaseObject;

/**
 * Class SyslogTarget
 * @package logger\targets
 */
class SyslogTarget extends BaseObject implements LogTargetInterface
{
    public $identity;

    public function export($content)
    {
        openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
        syslog(LOG_INFO, $content);
        closelog();
    }
}