<?php

namespace logger;

use logger\handlers\BasicHandler;
use logger\handlers\LogHandlerInterface;

/**
 * Class LogHandlerFactory
 * @package logger
 */
class LogHandlerFactory
{
    /**
     * Getting log handler
     *
     * @param string $method
     *
     * @return LogHandlerInterface
     */
    public function getHandler($method)
    {
        return new BasicHandler();
    }

}