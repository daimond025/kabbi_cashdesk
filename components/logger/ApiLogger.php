<?php

namespace logger;

use logger\targets\LogTargetInterface;
use yii\web\Request;
use yii\web\Response;

/**
 * Class ApiLogger
 * @package logger
 */
class ApiLogger
{
    /**
     * @var LogTargetInterface
     */
    private $target;

    /**
     * @var array
     */
    private $messages = [];

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var LogHandlerFactory
     */
    private $logHandlerFactory;

    /**
     * @var array
     */
    private static $startTime;

    /**
     * Getting system times
     *
     * @return array
     */
    private static function getSystemTimes()
    {
        $data = getrusage();

        return [
            microtime(true) * 1e6,
            $data['ru_utime.tv_sec'] * 1e6 + $data['ru_utime.tv_usec'],
            $data['ru_stime.tv_sec'] * 1e6 + $data['ru_stime.tv_usec'],
        ];
    }

    /**
     * Setup start time for statistic information
     */
    public static function setupStartTimeForStatistic()
    {
        self::$startTime = self::getSystemTimes();
    }

    /**
     * ApiLogger constructor.
     *
     * @param LogTargetInterface $target
     * @param LogHandlerFactory  $logHandlerFactory
     */
    public function __construct(LogTargetInterface $target, LogHandlerFactory $logHandlerFactory)
    {
        $this->target = $target;
        $this->logHandlerFactory = $logHandlerFactory;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Add message to log
     *
     * @param string $message
     * @param array  $context
     */
    public function log($message, array $context = [])
    {
        $this->messages[] = $this->interpolate($message, $context);
    }

    /**
     * Export log messages
     */
    public function export()
    {
        $this->target->export($this->getContent());
    }

    /**
     * Interpolate log message
     *
     * @param string $message
     * @param array  $context
     *
     * @return string
     */
    private function interpolate($message, array $context = [])
    {
        $replace = [];
        foreach ($context as $key => $val) {
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        return strtr($message, $replace);
    }

    /**
     * Getting value of server param
     *
     * @param string $param
     * @param string $defaultValue
     *
     * @return string
     */
    private function getServerParam($param, $defaultValue = '')
    {
        return array_key_exists($param, $_SERVER) ? $_SERVER[$param] : $defaultValue;
    }

    /**
     * @return string
     */
    private function getSender()
    {
        $ip = empty($this->getServerParam('HTTP_X_REAL_IP'))
            ? $this->getServerParam('REMOTE_ADDR', '???')
            : $this->getServerParam('HTTP_X_REAL_IP');

        $port = empty($this->getServerParam('HTTP_X_REAL_PORT'))
            ? $this->getServerParam('REMOTE_PORT', '???')
            : $this->getServerParam('HTTP_X_REAL_PORT');

        return implode(':', [$ip, $port]);
    }

    /**
     * Getting request statistic
     * @return string
     */
    private function getStatistic()
    {
        $size = $this->getServerParam('CONTENT_LENGTH', 0);

        list($time, $utime, $stime) = array_map(function ($a, $b) {
            return ($b - $a) / 1000.0;
        }, self::$startTime, self::getSystemTimes());

        $memory = round(memory_get_peak_usage() / 1024, 3);

        return sprintf('n:%db t:%.3fms u:%.3fms s:%.3fms m:%dkb', $size, $time, $utime, $stime, $memory);
    }

    /**
     * Getting current method
     * @return string
     */
    private function getCurrentMethod()
    {
        return strtok($this->getServerParam('REQUEST_URI'), '?');
    }

    /**
     * Getting information of request
     * @return string
     */
    private function getRequestInfo()
    {
        return implode(' ', [
            $this->getServerParam('REQUEST_METHOD'),
            $this->getCurrentMethod(),
            $this->getServerParam('SERVER_PROTOCOL'),
        ]);
    }

    /**
     * Getting information of response
     */
    private function getResponseInfo()
    {
        return $this->response->statusCode . ' ' . $this->response->statusText;
    }

    /**
     * Getting prepared result
     * @return string
     */
    private function getPreparedResult()
    {
        $handler = $this->logHandlerFactory->getHandler($this->getCurrentMethod());
        $content = $handler->process($this->request, $this->response)
            . (empty($this->messages) ? '' : ' (' . implode('; ', $this->messages) . ')');

        return $content;
    }

    /**
     * Getting content of log
     * @return string
     */
    private function getContent()
    {
        $sender = $this->getSender();
        $statistics = $this->getStatistic();
        $requestInfo = $this->getRequestInfo();
        $responseInfo = $this->getResponseInfo();
        $result = $this->getPreparedResult();

        return "{$sender} -> \"{$requestInfo}\": {$responseInfo} {$result} {$statistics}";
    }
}