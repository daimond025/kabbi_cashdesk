<?php

namespace app\components;

use app\exceptions\InvalidModelProfileIdException;
use app\services\ModulkassaService;
use app\services\OrangedataService;

class ReceiptServiceFactory
{
    /**
     * @param $type_id
     *
     * @return string
     * @throws InvalidModelProfileIdException
     */
    public static function getService($type_id)
    {
        switch ($type_id) {
            case 1:
                return ModulkassaService::class;

            case 2:
                return OrangedataService::class;

            default:
                throw new InvalidModelProfileIdException('Invalid type id: ' . $type_id);
        }
    }

}