<?php

namespace app\components;

use app\exceptions\InvalidModelProfileIdException;
use app\models\ModulkassaProfileModel;
use app\models\OrangedataProfileModel;

class ProfileModelFactory
{
    /**
     * @param $type_id
     *
     * @return string
     * @throws InvalidModelProfileIdException
     */
    public static function getModel($type_id)
    {
        switch ($type_id) {
            case 1:
                return ModulkassaProfileModel::class;

            case 2:
                return OrangedataProfileModel::class;

            default:
                throw new InvalidModelProfileIdException('Invalid type id: ' . $type_id);
        }
    }

}