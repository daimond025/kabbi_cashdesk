<?php

namespace app\components\creatorFile;

use Keboola\Temp\Temp;
use yii\base\Object;

/**
 * Class CreatorFile
 * @package app\components\creatorFile
 *
 */
class CreatorFile extends Object
{

    private $_creator;

    public function init()
    {
        $this->_creator = new Temp();
    }

    /**
     * @param $content
     *
     * @return \SplFileInfo
     * @throws CreatorFileException
     */
    public function createTmpFile($content)
    {
        $fileInfo = $this->createUniqueTmpFile();

        $file = $this->openFileOnWrite($fileInfo);
        if ($file->fwrite($content) < strlen($content)) {
            throw new CreatorFileException('Don\'t write content to the file');
        }

        return $fileInfo;
    }

    /**
     * @return \SplFileInfo
     * @throws CreatorFileException
     */
    protected function createUniqueTmpFile()
    {
        try {
            return $this->_creator->createTmpFile();
        } catch (\Exception $e) {
            throw new CreatorFileException('Don\'t create temp file', $e);
        }
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return \SplFileObject
     * @throws CreatorFileException
     */
    protected function openFileOnWrite(\SplFileInfo $fileInfo)
    {
        try {
            return $fileInfo->openFile('w+');
        } catch (\RuntimeException $e) {
            throw new CreatorFileException('Don\'t open file', $e);
        }
    }

}