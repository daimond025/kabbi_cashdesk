<?php

namespace healthCheck\checks;

/**
 * Class BaseCheck
 * @package healthCheck\checks
 */
abstract class BaseCheck
{
    /**
     * @var string
     */
    private $name;

    /**
     * BaseCheck constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    abstract public function run();

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}