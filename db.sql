-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 192.168.40.70:3306
-- Время создания: Окт 01 2019 г., 10:11
-- Версия сервера: 5.6.39-83.1-56-log
-- Версия PHP: 5.6.37-1+0~20180725093903.2+jessie~1.gbp606419

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `k_cashdesk`
--

-- --------------------------------------------------------

--
-- Структура таблицы `check`
--

CREATE TABLE `check` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `sending_condition` int(11) NOT NULL,
  `taxation_system` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1520841491),
('m180225_085907_create_table__type', 1520841491),
('m180225_090617_add_modulkassa_type', 1520841491),
('m180225_090939_create_table__profile', 1520841491),
('m180225_122305_create_table__configuration', 1520841491),
('m180225_191326_create_table__modulkassa_auth', 1520841491),
('m180228_184521_create_table__check', 1520841491),
('m180311_120534_modulkassa_auth__add_column__debug', 1520841491),
('m180312_092119_add_type_orangedata', 1522134596),
('m180312_111621_change_foreign_key__profile', 1520855604),
('m180312_125417_restruct_profile_table', 1522134596),
('m180313_102555_create_table__orangedata_profile', 1522134596),
('m180323_071117_edit__ssl_client_crt_pass__in_orangedata_profile', 1522134596);

-- --------------------------------------------------------

--
-- Структура таблицы `modulkassa_auth`
--

CREATE TABLE `modulkassa_auth` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `debug` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modulkassa_profile`
--

CREATE TABLE `modulkassa_profile` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `modulkassa_email` varchar(255) NOT NULL,
  `modulkassa_password` varchar(255) NOT NULL,
  `modulkassa_retail_point_uuid` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `orangedata_profile`
--

CREATE TABLE `orangedata_profile` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `inn` varchar(255) NOT NULL,
  `sign_pkey` mediumblob NOT NULL,
  `ssl_client_key` mediumblob NOT NULL,
  `ssl_client_crt` mediumblob NOT NULL,
  `ssl_ca_cert` mediumblob NOT NULL,
  `ssl_client_crt_pass` varchar(255) DEFAULT NULL,
  `payment_agent_operation` varchar(255) NOT NULL,
  `payment_transfer_operator_phones` text NOT NULL,
  `payment_agent_phones` text NOT NULL,
  `payment_operator_phones` text NOT NULL,
  `payment_operator_name` varchar(255) NOT NULL,
  `payment_operator_address` text NOT NULL,
  `payment_operator_inn` varchar(255) NOT NULL,
  `supplier_phones` text NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `debug` int(11) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'МодульКасса', NULL, NULL),
(2, 'Orange Data', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `check`
--
ALTER TABLE `check`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ui_configuration` (`tenant_id`,`city_id`,`position_id`),
  ADD KEY `fk_configuration__profile_id` (`profile_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `modulkassa_auth`
--
ALTER TABLE `modulkassa_auth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ui_modulkassa_auth` (`profile_id`,`debug`);

--
-- Индексы таблицы `modulkassa_profile`
--
ALTER TABLE `modulkassa_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ui_modulkassa_profile` (`profile_id`);

--
-- Индексы таблицы `orangedata_profile`
--
ALTER TABLE `orangedata_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ui_orangedata_profile` (`profile_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ui_profile` (`tenant_id`,`name`),
  ADD KEY `fk_profile__type_id` (`type_id`);

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `check`
--
ALTER TABLE `check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modulkassa_auth`
--
ALTER TABLE `modulkassa_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `modulkassa_profile`
--
ALTER TABLE `modulkassa_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `orangedata_profile`
--
ALTER TABLE `orangedata_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `configuration`
--
ALTER TABLE `configuration`
  ADD CONSTRAINT `fk_configuration__profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `modulkassa_auth`
--
ALTER TABLE `modulkassa_auth`
  ADD CONSTRAINT `fk_modulkassa_auth__profile_id` FOREIGN KEY (`profile_id`) REFERENCES `modulkassa_profile` (`profile_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `modulkassa_profile`
--
ALTER TABLE `modulkassa_profile`
  ADD CONSTRAINT `fk_modulkassa_profile__profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orangedata_profile`
--
ALTER TABLE `orangedata_profile`
  ADD CONSTRAINT `fk_orangedata_profile__profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_profile__type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
