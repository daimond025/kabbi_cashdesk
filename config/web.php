<?php

use yii\web\Response;

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/log_vars_filter.php'
);

$db = require __DIR__ . '/db.php';
$params = require __DIR__ . '/params.php';
$syslogIdentity = getenv('SYSLOG_IDENTITY') . '-' . $params['version'];

$config = [
    'id'         => 'api_cashdesk',
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => ['log'],
    'aliases'    => [
        '@healthCheck' => '@app/components/healthCheck',
        '@logger'      => '@app/components/logger',
    ],
    'components' => [
        'db'         => $db['dbMain'],
        'cache'      => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],
        'i18n'       => [
            'translations' => [
                'yii' => [
                    'class'            => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage'   => 'en',
                    'basePath'         => '@app/messages',
                    'forceTranslation' => true,
                ],
            ],
        ],
        'log'        => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'levels'  => ['error'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:404',
                    ],
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
            ],
        ],
        'mailer'     => [
            'class'     => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],
        'request'    => [
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response'   => [
            'formatters'    => [
                Response::FORMAT_JSON => [
                    'class'         => 'yii\web\JsonResponseFormatter',
                    'prettyPrint'   => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            'on beforeSend' => ['\app\handlers\BeforeSendResponseHandler', 'handle'],
            'on afterSend'  => ['app\handlers\AfterSendResponseHandler', 'handle'],
        ],
        'user'       => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/routes.php',
        ],

        // Other components and services
        'apiLogger'  => '\logger\ApiLogger',
    ],

    'container' => [
        'definitions' => [
            'GuzzleHttp\Client'                 => [
                'timeout' => $params['timeout'],
            ],
            'logger\targets\LogTargetInterface' => [
                'class'    => 'logger\targets\SyslogTarget',
                'identity' => $syslogIdentity,
            ],
        ],
    ],

    'on beforeRequest' => ['logger\ApiLogger', 'setupStartTimeForStatistic'],

    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['192.168.1.250', '127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['192.168.1.250', '127.0.0.1', '::1'],
    ];
}

return $config;
