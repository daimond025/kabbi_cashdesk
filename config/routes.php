<?php

return [
    'version' => '/api/version',
    'status'  => '/api/status',

    [
        'class'      => 'yii\rest\UrlRule',
        'prefix'     => 'api/v1',
        'controller' => 'type',
        'only'       => ['index'],
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'prefix'     => 'api/v1',
        'controller' => 'profile',
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'prefix'     => 'api/v1',
        'controller' => 'configuration',
    ],
    [
        'class'         => 'yii\rest\UrlRule',
        'prefix'        => 'api/v1',
        'controller'    => 'receipt',
        'only'          => [],
        'extraPatterns' => [
            'POST send' => 'send',
        ],
    ],
];