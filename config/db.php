<?php

return [
    'dbMain'     => [
        'class'               => 'yii\db\Connection',
        'charset'             => 'utf8',
        'dsn'                 => getenv('MYSQL_DSN'),
        'username'            => getenv('MYSQL_USERNAME'),
        'password'            => getenv('MYSQL_PASSWORD'),
        'enableSchemaCache'   => getenv('DB_MAIN_SCHEMA_CACHE_ENABLE') ? true : false,
        'schemaCacheDuration' => (int)getenv('DB_MAIN_SCHEMA_CACHE_DURATION'),
    ],
    'dbTest'     => [
        'class'             => 'yii\db\Connection',
        'charset'           => 'utf8',
        'dsn'               => getenv('MYSQL_TEST_DSN'),
        'username'          => getenv('MYSQL_TEST_USERNAME'),
        'password'          => getenv('MYSQL_TEST_PASSWORD'),
        'enableSchemaCache' => false
    ],
    'redisCache' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_MAIN'),
    ],
];


