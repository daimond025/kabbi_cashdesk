<?php

namespace app\controllers;

use healthCheck\CheckResult;
use healthCheck\checks\BaseCheck;
use healthCheck\checks\MySqlCheck;
use healthCheck\HealthCheckService;
use yii\base\Application;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{
    const SERVICE_OPERATIONAL = 'SERVICE OPERATIONAL';
    const SERVICE_NOT_OPERATIONAL = 'SERVICE NOT OPERATIONAL';

    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';

    const STATUS_LABELS = [
        self::STATUS_OK    => '[ OK    ]',
        self::STATUS_ERROR => '[ ERROR ]',
    ];

    const MIME_TYPE_TEXT_PLAIN = 'text/plain';

    /**
     * @var Application
     */
    private $application;

    /**
     * ApiController constructor.
     *
     * @param string $id
     * @param Module $module
     * @param array  $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->application = \Yii::$app;
    }

    /**
     * @return string
     */
    private function getApiVersion()
    {
        $application = \Yii::$app->id;
        $version = \Yii::$app->params['version'];

        return "{$application} v{$version}";
    }

    /**
     * @return string
     */
    private function getPHPVersion()
    {
        $php = PHP_VERSION;

        return "work on php v{$php}";
    }

    /**
     * @return bool
     */
    private function isTextFormatRequested()
    {
        return array_key_exists(self::MIME_TYPE_TEXT_PLAIN, $this->application->request->acceptableContentTypes);
    }

    /**
     * @param bool $textFormat
     */
    private function setResponseFormat($textFormat)
    {
        $this->application->response->format = $textFormat ? Response::FORMAT_HTML : Response::FORMAT_RAW;
    }

    /**
     * @return BaseCheck[]
     * @throws InvalidConfigException
     */
    private function getHealthChecks()
    {
        $application = $this->application;

        return [
            new MySqlCheck('database "db"', $application->get('db')),
        ];
    }

    public function actionVersion()
    {
        $result = implode(PHP_EOL, [$this->getApiVersion(), $this->getPHPVersion()]);

        $isTextFormatRequested = $this->isTextFormatRequested();
        $this->setResponseFormat($isTextFormatRequested);

        return $isTextFormatRequested
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

    public function actionStatus()
    {
        $status = self::STATUS_OK;
        $lines = [$this->getApiVersion(), $this->getPHPVersion(), ''];

        $checkResults = (new HealthCheckService($this->getHealthChecks()))->getCheckResults();
        foreach ($checkResults as $checkResult) {
            /* @var $checkResult CheckResult */
            if ($checkResult->isSuccessful()) {
                $lines[] = self::STATUS_LABELS[self::STATUS_OK] . " {$checkResult->getName()}";
            } else {
                $lines[] = self::STATUS_LABELS[self::STATUS_ERROR] . " {$checkResult->getName()} ({$checkResult->getErrorMessage()})";
                $status = self::STATUS_ERROR;
            }
        }

        $lines[] = '';
        $lines[] = $status === self::STATUS_OK ? self::SERVICE_OPERATIONAL : self::SERVICE_NOT_OPERATIONAL;

        $result = implode(PHP_EOL, $lines);

        $isTextFormatRequested = $this->isTextFormatRequested();
        $this->setResponseFormat($isTextFormatRequested);

        return $isTextFormatRequested
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

}
