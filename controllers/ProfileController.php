<?php

namespace app\controllers;

use app\components\ProfileModelFactory;
use app\exceptions\ValidationException;
use app\models\Profile;
use app\models\ProfileModelInterface;
use app\requests\IndexProfileRequest;
use yii\web\NotFoundHttpException;

class ProfileController extends BaseController
{
    public function actionIndex()
    {
        $request = \Yii::createObject(IndexProfileRequest::class);

        $request->attributes = \Yii::$app->request->get();
        if (!$request->validate()) {
            throw new ValidationException($request->getFirstErrors());
        }

        $profiles = Profile::find()->where(['tenant_id' => $request->tenant_id])->all();
        $response = [];

        /** @var Profile $profile */
        foreach ($profiles as $profile) {
            $modelClass = ProfileModelFactory::getModel($profile->type_id);
            $response[] = $modelClass::get($profile);
        }

        return $response;
    }

    public function actionView($id)
    {
        return $this->findModel($id);
    }

    public function actionCreate()
    {
        $modelClass = ProfileModelFactory::getModel(\Yii::$app->request->post('type_id'));

        $model             = \Yii::createObject($modelClass);
        $model->attributes = \Yii::$app->request->post();
        if (!$model->save()) {
            throw new ValidationException($model->getFirstErrors());
        }

        return $model;
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->scenario   = Profile::SCENARIO_UPDATE;
        $model->attributes = \Yii::$app->request->post();
        if (!$model->save()) {
            throw new ValidationException($model->getFirstErrors());
        }

        return $model;
    }

    public function actionDelete($id)
    {
        $profile = Profile::findOne($id);
        if ($profile !== null) {
            /** @var ProfileModelInterface $modelClass */
            $modelClass = ProfileModelFactory::getModel($profile->type_id);
            $model      = $modelClass::get($profile);
            $model->delete();
        }
    }

    private function findModel($id)
    {
        $profile = Profile::findOne($id);
        if ($profile === null) {
            throw new NotFoundHttpException('Profile not found');
        }

        /** @var ProfileModelInterface $modelClass */
        $modelClass = ProfileModelFactory::getModel($profile->type_id);

        return $modelClass::get($profile);
    }

}