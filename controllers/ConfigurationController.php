<?php

namespace app\controllers;

use app\exceptions\ValidationException;
use app\models\Configuration;
use app\requests\IndexConfigurationRequest;
use yii\web\NotFoundHttpException;

class ConfigurationController extends BaseController
{
    public function actionIndex()
    {
        $request = \Yii::createObject(IndexConfigurationRequest::class);

        $request->attributes = \Yii::$app->request->get();
        if (!$request->validate()) {
            throw new ValidationException($request->getFirstErrors());
        }

        return Configuration::find()
            ->where(['tenant_id' => $request->tenant_id])
            ->andFilterWhere(['city_id' => $request->city_id])
            ->andFilterWhere(['position_id' => $request->position_id])
            ->all();
    }

    public function actionView($id)
    {
        return $this->findModel($id);
    }

    public function actionCreate()
    {
        $model = \Yii::createObject(Configuration::class);

        $model->attributes = \Yii::$app->request->post();
        if (!$model->save()) {
            throw new ValidationException($model->getFirstErrors());
        }

        return $model;
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->attributes = \Yii::$app->request->post();
        if (!$model->save()) {
            throw new ValidationException($model->getFirstErrors());
        }

        return $model;
    }

    public function actionDelete($id)
    {
        $model = Configuration::findOne($id);
        if ($model !== null) {
            $model->delete();
        }
    }

    private function findModel($id)
    {
        $model = Configuration::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Configuration not found');
        }

        return $model;
    }

}