<?php

namespace app\controllers;

use app\models\Type;

class TypeController extends BaseController
{
    public function actionIndex()
    {
        return Type::find()->all();
    }
}