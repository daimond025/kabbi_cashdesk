<?php

namespace app\controllers;

use app\components\ReceiptServiceFactory;
use app\exceptions\ValidationException;
use app\models\Configuration;
use app\requests\SendReceiptRequest;
use app\services\ReceiptServiceInterface;
use yii\rest\Controller;

class ReceiptController extends Controller
{
    public function actionSend()
    {
        /** @var SendReceiptRequest $request */
        $request = \Yii::createObject(SendReceiptRequest::class);

        $request->attributes = \Yii::$app->request->post();
        if (!$request->validate()) {
            throw new ValidationException($request->getFirstErrors());
        }

        $configuration = Configuration::get($request->tenant_id, $request->city_id, $request->position_id);
        if ($configuration !== null) {
            $serviceClass = ReceiptServiceFactory::getService($configuration->profile->type_id);
            /** @var ReceiptServiceInterface $service */
            $service = \Yii::createObject($serviceClass, [\Yii::$app->get('apiLogger')]);
            $service->send($configuration, $request);
        }
    }
}