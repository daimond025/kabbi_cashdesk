<?php

use yii\db\Migration;

/**
 * Class m180313_102555_create_table__orangedata_profile
 */
class m180313_102555_create_table__orangedata_profile extends Migration
{
    private const TABLE_PROFILE = '{{%profile}}';
    private const TABLE_ORANGEDATA_PROFILE = '{{%orangedata_profile}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_ORANGEDATA_PROFILE, [
            'id'                               => $this->primaryKey(),
            'profile_id'                       => $this->integer(11)->notNull(),
            'inn'                              => $this->string()->notNull(),
            'sign_pkey'                        => 'MEDIUMBLOB NOT NULL',
            'ssl_client_key'                   => 'MEDIUMBLOB NOT NULL',
            'ssl_client_crt'                   => 'MEDIUMBLOB NOT NULL',
            'ssl_ca_cert'                      => 'MEDIUMBLOB NOT NULL',
            'ssl_client_crt_pass'              => $this->string()->notNull(),
            'payment_agent_operation'          => $this->string()->notNull(),
            'payment_transfer_operator_phones' => $this->text()->notNull(),
            'payment_agent_phones'             => $this->text()->notNull(),
            'payment_operator_phones'          => $this->text()->notNull(),
            'payment_operator_name'            => $this->string()->notNull(),
            'payment_operator_address'         => $this->text()->notNull(),
            'payment_operator_inn'             => $this->string()->notNull(),
            'supplier_phones'                  => $this->text()->notNull(),
            'created_at'                       => $this->integer(),
            'updated_at'                       => $this->integer(),
        ]);

        $this->createIndex('ui_orangedata_profile', self::TABLE_ORANGEDATA_PROFILE, 'profile_id', true);
        $this->addForeignKey('fk_orangedata_profile__profile_id', self::TABLE_ORANGEDATA_PROFILE, 'profile_id',
            self::TABLE_PROFILE, 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_ORANGEDATA_PROFILE);
    }
}
