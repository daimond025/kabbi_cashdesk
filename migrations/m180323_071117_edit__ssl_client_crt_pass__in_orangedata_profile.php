<?php

use yii\db\Migration;

/**
 * Class m180323_071117_edit__ssl_client_crt_pass__in_orangedata_profile
 */
class m180323_071117_edit__ssl_client_crt_pass__in_orangedata_profile extends Migration
{

    private const TABLE_NAME = '{{%orangedata_profile}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'ssl_client_crt_pass', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
