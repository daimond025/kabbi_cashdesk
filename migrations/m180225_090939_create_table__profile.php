<?php

use yii\db\Migration;

/**
 * Class m180225_090939_create_table__profile
 */
class m180225_090939_create_table__profile extends Migration
{
    private const TABLE_PROFILE = '{{%profile}}';
    private const TABLE_TYPE = '{{%profile}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_PROFILE, [
            'id'                           => $this->primaryKey(),
            'tenant_id'                    => $this->integer()->notNull(),
            'name'                         => $this->string()->notNull(),
            'type_id'                      => $this->integer()->notNull(),
            'debug'                        => $this->integer()->notNull()->defaultValue(1),
            'modulkassa_email'             => $this->string()->notNull(),
            'modulkassa_password'          => $this->string()->notNull(),
            'modulkassa_retail_point_uuid' => $this->string()->notNull(),
            'created_at'                   => $this->integer(),
            'updated_at'                   => $this->integer(),
        ]);

        $this->createIndex('ui_profile', self::TABLE_PROFILE, ['tenant_id', 'name'], true);
        $this->addForeignKey('fk_profile__type_id', self::TABLE_PROFILE, 'type_id', self::TABLE_TYPE, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_PROFILE);
    }

}
