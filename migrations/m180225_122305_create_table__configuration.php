<?php

use yii\db\Migration;

/**
 * Class m180225_122305_create_table__configuration
 */
class m180225_122305_create_table__configuration extends Migration
{
    private const TABLE_CONFIGURATION = '{{%configuration}}';
    private const TABLE_PROFILE = '{{%profile}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_CONFIGURATION, [
            'id'                => $this->primaryKey(),
            'tenant_id'         => $this->integer()->notNull(),
            'city_id'           => $this->integer()->notNull(),
            'position_id'       => $this->integer()->notNull(),
            'profile_id'        => $this->integer()->notNull(),
            'product_name'      => $this->string()->notNull(),
            'sending_condition' => $this->integer()->notNull(),
            'taxation_system'   => $this->integer()->notNull(),
            'tax'               => $this->integer()->notNull(),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ]);

        $this->createIndex('ui_configuration', self::TABLE_CONFIGURATION, ['tenant_id', 'city_id', 'position_id'],
            true);
        $this->addForeignKey('fk_configuration__profile_id', self::TABLE_CONFIGURATION, 'profile_id',
            self::TABLE_PROFILE, 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_CONFIGURATION);
    }

}
