<?php

use yii\db\Migration;

/**
 * Class m180225_090617_add_modulkassa_type
 */
class m180225_090617_add_modulkassa_type extends Migration
{
    private const TABLE_NAME = '{{%type}}';
    private const MODULKASSA_TYPE_ID = 1;

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'id'   => self::MODULKASSA_TYPE_ID,
            'name' => 'МодульКасса',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['id' => self::MODULKASSA_TYPE_ID]);
    }

}
