<?php

use yii\db\Migration;

/**
 * Class m180225_085907_create_table__type
 */
class m180225_085907_create_table__type extends Migration
{
    private const TABLE_NAME = '{{%type}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
