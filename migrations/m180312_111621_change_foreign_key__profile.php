<?php

use yii\db\Migration;

/**
 * Class m180312_111621_change_foreign_key__profile
 */
class m180312_111621_change_foreign_key__profile extends Migration
{

    private const TABLE_PROFILE = '{{%profile}}';
    private const TABLE_TYPE = '{{%type}}';

    private const FOREIGN_KEY_NAME = 'fk_profile__type_id';

    private const COLUMN_NAME_PROFILE = 'type_id';
    private const COLUMN_NAME_TYPE = 'id';

    public function safeUp()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_PROFILE);
        $this->addForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_PROFILE, self::COLUMN_NAME_PROFILE, self::TABLE_TYPE, self::COLUMN_NAME_TYPE, 'CASCADE');
    }


    public function safeDown()
    {
        return true;
    }

}
