<?php

use yii\db\Migration;

/**
 * Class m180312_125417_restruct_profile_table
 */
class m180312_125417_restruct_profile_table extends Migration
{
    private const TABLE_PROFILE = '{{%profile}}';
    private const TABLE_MODULKASSA_PROFILE = '{{%modulkassa_profile}}';
    private const TABLE_MODULKASSA_AUTH = '{{%modulkassa_auth}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_MODULKASSA_PROFILE, [
            'id'                           => $this->primaryKey(),
            'profile_id'                   => $this->integer(11)->notNull(),
            'modulkassa_email'             => $this->string()->notNull(),
            'modulkassa_password'          => $this->string()->notNull(),
            'modulkassa_retail_point_uuid' => $this->string()->notNull(),
            'created_at'                   => $this->integer(),
            'updated_at'                   => $this->integer(),
        ]);

        $this->createIndex('ui_modulkassa_profile', self::TABLE_MODULKASSA_PROFILE, 'profile_id', true);
        $this->addForeignKey('fk_modulkassa_profile__profile_id', self::TABLE_MODULKASSA_PROFILE, 'profile_id',
            self::TABLE_PROFILE, 'id', 'CASCADE', 'CASCADE');

        $profiles = $this->getProfiles();
        $columns  = [
            'profile_id',
            'modulkassa_email',
            'modulkassa_password',
            'modulkassa_retail_point_uuid',
            'created_at',
            'updated_at',
        ];

        $insert = [];
        foreach ($profiles as $profile) {
            $insert[] = [
                'profile_id'                   => $profile['id'],
                'modulkassa_email'             => $profile['modulkassa_email'],
                'modulkassa_password'          => $profile['modulkassa_password'],
                'modulkassa_retail_point_uuid' => $profile['modulkassa_retail_point_uuid'],
                'created_at'                   => $profile['created_at'],
                'updated_at'                   => $profile['updated_at'],
            ];
        }

        $insertChunk = array_chunk($insert, 100);
        foreach ($insertChunk as $chunk) {
            $this->batchInsert(self::TABLE_MODULKASSA_PROFILE, $columns, $chunk);
        }

        $this->dropForeignKey('fk_modulkassa_auth__profile_id', self::TABLE_MODULKASSA_AUTH);
        $this->addForeignKey('fk_modulkassa_auth__profile_id', self::TABLE_MODULKASSA_AUTH, 'profile_id',
            self::TABLE_MODULKASSA_PROFILE, 'profile_id', 'CASCADE');

        $this->dropColumn(self::TABLE_PROFILE, 'modulkassa_email');
        $this->dropColumn(self::TABLE_PROFILE, 'modulkassa_password');
        $this->dropColumn(self::TABLE_PROFILE, 'modulkassa_retail_point_uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(self::TABLE_PROFILE, 'modulkassa_email', $this->string()->notNull()->after('debug'));
        $this->addColumn(self::TABLE_PROFILE, 'modulkassa_password', $this->string()->notNull()->after('modulkassa_email'));
        $this->addColumn(self::TABLE_PROFILE, 'modulkassa_retail_point_uuid', $this->string()->notNull()->after('modulkassa_password'));

        $profiles = $this->getModulkassaProfiles();

        foreach($profiles as $profile) {
            $columns  = [
                'modulkassa_email' => $profile['modulkassa_email'],
                'modulkassa_password' => $profile['modulkassa_password'],
                'modulkassa_retail_point_uuid' => $profile['modulkassa_retail_point_uuid'],
            ];
            $this->update(self::TABLE_PROFILE, $columns, ['id' => $profile['profile_id']]);
        }

        $this->dropForeignKey('fk_modulkassa_auth__profile_id', self::TABLE_MODULKASSA_AUTH);
        $this->addForeignKey('fk_modulkassa_auth__profile_id', self::TABLE_MODULKASSA_AUTH, 'profile_id',
            self::TABLE_PROFILE, 'id', 'CASCADE');

        $this->dropTable(self::TABLE_MODULKASSA_PROFILE);
    }


    private function getProfiles()
    {
        return (new \yii\db\Query())->from(self::TABLE_PROFILE)->all();
    }

    private function getModulkassaProfiles()
    {
        return (new \yii\db\Query())->from(self::TABLE_MODULKASSA_PROFILE)->all();
    }

}
