<?php

use yii\db\Migration;

/**
 * Class m180312_092119_add_type_orangedata
 */
class m180312_092119_add_type_orangedata extends Migration
{
    private const TABLE_NAME = '{{%type}}';
    private const ORANGEDATA_TYPE_ID = 2;

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'id'   => self::ORANGEDATA_TYPE_ID,
            'name' => 'Orange Data',
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['id' => self::ORANGEDATA_TYPE_ID]);
    }
}
