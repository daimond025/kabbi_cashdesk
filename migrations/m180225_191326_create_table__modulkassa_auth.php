<?php

use yii\db\Migration;

/**
 * Class m180225_191326_crate_table__modulkassa_auth
 */
class m180225_191326_create_table__modulkassa_auth extends Migration
{
    private const TABLE_NAME = '{{%modulkassa_auth}}';
    private const TABLE_PROFILE = '{{%profile}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'profile_id' => $this->integer()->notNull()->unique(),
            'username'   => $this->string()->notNull(),
            'password'   => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_modulkassa_auth__profile_id', self::TABLE_NAME, 'profile_id',
            self::TABLE_PROFILE, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
