<?php

use yii\db\Migration;

/**
 * Class m180228_184521_create_table__check
 */
class m180228_184521_create_table__check extends Migration
{
    const TABLE_NAME = 'check';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'    => $this->primaryKey(),
            'key'   => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
