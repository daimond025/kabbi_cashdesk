<?php

use yii\db\Migration;

/**
 * Class m180311_120534_modulkassa_auth__add_column__debug
 */
class m180311_120534_modulkassa_auth__add_column__debug extends Migration
{
    const TABLE_NAME = '{{%modulkassa_auth}}';
    const COLUMN_DEBUG = 'debug';
    const UNIQUE_INDEX_PROFILE_ID = 'profile_id';
    const UNIQUE_INDEX_PROFILE_ID_DEBUG = 'ui_modulkassa_auth';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_DEBUG, $this->integer()->notNull()->defaultValue(0));
        $this->createIndex(self::UNIQUE_INDEX_PROFILE_ID_DEBUG, self::TABLE_NAME, ['profile_id', 'debug'], true);
        $this->dropIndex(self::UNIQUE_INDEX_PROFILE_ID, self::TABLE_NAME);
    }

    public function safeDown()
    {
        $this->createIndex(self::UNIQUE_INDEX_PROFILE_ID, self::TABLE_NAME, 'profile_id', true);
        $this->dropIndex(self::UNIQUE_INDEX_PROFILE_ID_DEBUG, self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_DEBUG);
    }
}
