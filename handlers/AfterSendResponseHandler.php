<?php

namespace app\handlers;

use logger\ApiLogger;

class AfterSendResponseHandler
{
    public static function handle()
    {
        /** @var $apiLogger ApiLogger */
        $apiLogger = \Yii::$app->apiLogger;
        $apiLogger->setRequest(\Yii::$app->request);
        $apiLogger->setResponse(\Yii::$app->response);
        $apiLogger->export();
    }
}