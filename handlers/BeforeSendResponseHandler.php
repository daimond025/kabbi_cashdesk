<?php

namespace app\handlers;

class BeforeSendResponseHandler
{
    public static function handle($event)
    {
        $response = $event->sender;
        if ($response->statusCode >= 400) {
            $messages = explode(';', $response->data['message'] ?? '');

            $errors = [];
            foreach ($messages as $message) {
                $parts = explode('|', $message);
                $errors[] = [
                    'code'    => isset($parts[1]) ? $parts[0] : 'not specified',
                    'message' => $parts[1] ?? $parts[0],
                ];
            }

            $response->data = ['errors' => $errors];
        }
    }
}
